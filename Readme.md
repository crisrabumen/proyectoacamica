# Acamica SprintProject 1


Proyecto para cerrar es primer sprint. 

## Instalación

Descargar el repositorio e instalar los package del proyeto como se muestra a continuación.

```bash
npm install 
```

## Uso

## Pre Uso.


*IMPORTANTE:
-Activar servicio Mongo DB
-Activar servicio redis para chache

-Verificar en el archivo .env los puertos escuchas y en caso de ser necesarios cambiar la coneccion para evitar inconvenientes.

*Ejecuta los  siguiente comandos

 -Tutina de test: 
 npm test

*Para correr el programa:

```JavaScript
nodemon src/index.js

```
Entrar en la documentación de swagger en la url: [Documentation](http://localhost:3000/api-docs/)

## Loguear y Registro


## Loguear 
Usuario administrador:

user: admin---- pass: 12345

Usuarios:

* user: usuario---- pass: 12345
* user: parapedido---- pass: 12345

## Registro

El registro permite ver en el Get todos los usuarios registrados por defecto en el sistema.

Para registrar un nuevo usuario utilizar el Post... Ningún usuario nuevo puede ser administrador.

## Productos

Para poder utilizar las funcionalidades del producto debe estar logueado en el sistema, como admin o como usuario.

Los usuarios únicamente pueden ver la lista de todos los productos registrados en el sistema.

Los administradores tienen habilitada la función de agregar, actualizar o borrar productos. 

## Agregar Productos

Para agregar un nuevo producto debe colocar el "id" manualmente, y debe verificar que esté sea la misma posición de dicho producto en el array. Si otro producto tiene el mismo "id" o esta en la posición del array que corresponde al "id" al id del nuevo producto, este no podrá ser agregado. 

En el body deben ir los siguientes datos en forma de objeto, por ejemplo:

{

  "names": "Salchipapa",

  "price": 12000
}

## Actualizar Productos

Las actualizaciones se realizan por medio del id y se envían utilizando parametros.

Para actualizar un producto, debe enviar el id de este que debe ser igual a su posición en el array, y enviar por el body un nuevo objeto con los datos actualziados, por ejemplo:

{

  "names": "SalchipapaMixta",

  "price": 20000
}

## Eliminar Productos

La eliminación de un producto se realiza por medio del id y se envía utilizando parametros.

Para eliminar un producto, debe enviar el id de este que debe ser igual a su posición en el array.


## Pedidos

Para poder utilizar las funcionalidades de pedidos debe estar logueado en el sistema, como admin o como usuario.

Get:

Los usuarios únicamente pueden ver la lista de los pedidos asignados a su nombre.

Los administradores pueden ver la lista de todos los pedidos en la historia.


## Agregar Pedidos

Para realizar un pedido un usuario no debe tener pedidos pendientes, y debe utilizar el body para mandar la información del pedido de la siguiente manera:

{
  "productos": [
    ["Chuleta",1], 
    ["Coca Cola 500ml",1]
  ],

  "address": "calle2231",

  "formaPago": "Efectivo"
}

Todos los pedidos creados por primera vez, se les asigna un estado de pedido igual a "Pendiente", por defecto, para confirmar pedido el usuario debe hacer un put.

## Modificar Pedidos

Todas las modificaciones de los pedidos se realizan por medio de la query, con el id del pedido, el cual es asignado por el programa una vez se realiza el pedido y este nunca cambia.

## Para confirmar un pedido:

si el usuario únicamente desea confirmar su pedido, debe enviar por medio de parametros el id de su pedido, y debe enviar en el body un array de producto exactamente igual al que envio por el post, mas un ítem string llamado "estadoProducto" donde debe enviar la palabra "Confirmado"... Por ejemplo:

{
  "productos": [
    ["Chuleta",1]
  ],

  "estadoPedido": "Confirmado"
}

## Para modificar un pedido:

Los usuarios podrán modificar sus pedidos únicamente cuando el estado de estos sea igual a "Pendiente", Y para hacerlo deberán mandar el id de sus pedido por medio parametros, y debe enviar en el body un array de producto actualizando sus cambios, es decir borrar dejar unicamente los productos que desea comprar o agregar uno nuevo. Mas un ítem string llamado "estadoProducto" donde debe enviar la palabra "Confirmado"... Por ejemplo:

Supongamos que el usuario desea agregar un nuevo producto llamado "Picada" a su pedido:

{
   "productos": [
    ["Chuleta",1]
  ],

  "estadoPedido": "Confirmado"
}

Supongamos que el usuario desea eliminar un  producto de su pedido o reemplazarlo por otro:

{
   "productos": [
    ["Chuleta",1]
  ],

  "estadoPedido": "Confirmado"
}

El array enviado desde el body reemplazará completamente el array anterior.

## Para modificar un pedido Admin:

Los administradores únicamente pueden modificar los estados de los pedidos.
Enviando el id de estos por medio parametros y enviando el nuevo estado por el body:

{
  "estadoPedido": "En Preparación"
}

## Métodos de pago

Todos los usuarios registrados deben estar logueados y pueden ver por el get, los métodos de pago habilitados por el sistema.

Únicamente los administradores pueden agregar, modificar o borrar métodos de pago.

## Agregar Métodos de pago

El administrador debe enviar por medio del body un objeto con le nuevo metodo de pago, el id del metodo de pago se asigna automáticamente por el body :
{
  "formaPago": "Debito"
}

## Modificar Métodos de pago

El administrador debe enviar el id del método de pago por la query, que corresponde obligatoriamente a su posición en el array, y la actualización de este por medio del body: 

{
  "formaPago": "Pago electronico"
}

## Borrar Métodos de pago

El administrador debe enviar por la query el id, del método de pago que desea borrar. Se recomienda no borrar métodos de pago y en su lugar actualizarlos, para evitar inconvenientes con el id de estos, en caso de ser necesario solo borrar el que se encuentra en la ultima posición y actualizar el nombre los demás, sin embargo, cualquiera puede ser borrado.

## Angenda de direcciones

Los usuarios ahora podran tener una agenda de direcciones, la cual podran consultar para elegir una y utilizarla para sus pedidos, o tambien podran agregar una nueva direccion en caso de ser necesario.

-Para agregar direcciones deben estar logeados y hacerlo mediante un body de la siguiente manera:

    {
      "direccion": "calle 11 carrera 12"
    }

## 



## License
API en desarrollo